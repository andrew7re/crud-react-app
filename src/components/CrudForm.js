import { useState, useEffect } from 'react'

const Crudform = (props) => {

    useEffect(() => {
        setCurrentUser(props.users.map(user => ({

            id: user._id,
            name: user.data.name,
            email: user.data.email,
            login: user.data.login,
            phone: user.data.phone,
            age: user.data.age,
            address: user.data.address

        })))
    }, [props])
    const [currentUser, setCurrentUser] = useState(props.users.map(user => ({

        id: user._id,
        name: user.data.name,
        email: user.data.email,
        login: user.data.login,
        phone: user.data.phone,
        age: user.data.age,
        address: user.data.address

    })))
    // console.log(currentUser)
    const [userId, setUserId] = useState('')
    const [editing, setEditing] = useState({})

    const editUser = (id, user) => {
        setUserId(id)
        setEditing(user)
    }
    const handleChange = e => {
        const { name, value } = e.target
        setEditing({ ...editing, [name]: value })
        setCurrentUser(currentUser.map((user) => (user.id === userId ? { ...user, [name]: value } : user))
        )
        // console.log(currentUser)
        // // console.log(editing)
        
    }

    const handleSubmit = e => {
        e.preventDefault()
        handleChange(e, props.updateUser(userId, editing),setCurrentUser(currentUser.map((user) => (user.id === userId ? editing : user))
        ))
        setUserId('')
        
    }
    
    return (
        <table className='table table-striped mb-0 table-bordered'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Login</th>
                    <th>Phone</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {currentUser ? (
                    currentUser.map(user => {
                        return (
                            <tr key={user.id}>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.name : user.name} name="name" onChange={handleChange} /></td>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.email : user.email} name="email" onChange={handleChange} /></td>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.login : user.login} name="login" onChange={handleChange} /></td>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.phone : user.phone} name="phone" onChange={handleChange} /></td>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.age : user.age} name="age" onChange={handleChange} /></td>
                                <td><input className="form-control" type="text" readOnly={editing && user.id === userId ? false : true} value={editing && user.id === userId ? editing.address : user.address} name="address" onChange={handleChange} /></td>
                                {/* <td>{user.data.name}</td>
                                <td>{user.data.email}</td>
                                <td>{user.data.login}</td>
                                <td>{user.data.phone}</td>
                                <td>{user.data.age}</td>
                                <td>{user.data.address}</td> */}
                                <td>
                                    {user.id === userId ? (
                                        <button className="btn btn-success" type="submit" onClick={handleSubmit} >Save</button>

                                    ) : (
                                        <button className='btn btn-primary' onClick={() => editUser(user.id, user)}>Edit</button>
                                    )}
                                    <button className='btn btn-danger' onClick={() => props.deleteUser(user.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })
                ) : (
                    <tr>
                        <td colSpan={7}>No users found</td>
                    </tr>
                )
                }
            </tbody>
        </table>
    )
}

export default Crudform
