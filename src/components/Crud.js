import axios from "axios";
import { useState, useEffect } from 'react'
import AddUserForm from "./AddUserForm"
import CrudForm from "./CrudForm"

const Crud = () => {
    const [users, setUsers] = useState([])

    useEffect(() => {
        const getUsers = async () => {
            await axios.get('http://178.128.196.163:3000/api/records')
                .then((res) => {
                    setUsers(res.data)
                })
                .catch((err) => {
                    console.log(err)
                })
        }
        getUsers()
    }, [users])

    const addUser = async (user) => {
        if (user) {
            await axios.put('http://178.128.196.163:3000/api/records', { data: user })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    const deleteUser = async (id) => {
        if (id) {
            await axios.delete(`http://178.128.196.163:3000/api/records/${id}`)
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    const updateUser = async (id, user) => {
        if (id && user) {
            await axios.post(`http://178.128.196.163:3000/api/records/${id}`, { data: user })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    return (
        <div className="container">
            <h1>React CRUD App </h1>
            <div className="row">
                <div className="col-2">
                        <div>
                            <h2>Add user</h2>
                            <AddUserForm addUser={addUser} />
                        </div>
                </div>
                <div className="col-10 table-wrapper-scroll-y hdd50cv">
                    <h2>View users</h2>
                    <CrudForm
                        users={users}
                        deleteUser={deleteUser}
                        updateUser={updateUser}
                    />
                </div>
            </div>
        </div>
    )
}

export default Crud
