import { useState } from 'react'

const EditUserForm = (props) => {

    const [user, setUser] = useState(props.currentUser);

    const handleChange = e => {
        const { name, value } = e.target
        setUser({ ...user, [name]: value })
    }

    const handleSubmit = e => {
        e.preventDefault()
        handleChange(e, props.updateUser(props.userId, user))
    }

    return (
        <form>
            <label>Name</label>
            <input className="form-control" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Email</label>
            <input className="form-control" type="text" value={user.email} name="email" onChange={handleChange} />
            <label>Login</label>
            <input className="form-control" type="text" value={user.login} name="login" onChange={handleChange} />
            <label>Phone</label>
            <input className="form-control" type="text" value={user.phone} name="phone" onChange={handleChange} />
            <label>Age</label>
            <input className="form-control" type="text" value={user.age} name="age" onChange={handleChange} />
            <label>Adress</label>
            <input className="form-control" type="text" value={user.address} name="address" onChange={handleChange} />
            <button className="btn btn-success" type="submit" onClick={handleSubmit} >Save</button>
            <button className="btn btn-danger" type="submit" onClick={() => props.setEditing(false)} >Cancel</button>
        </form>
    )
}

export default EditUserForm