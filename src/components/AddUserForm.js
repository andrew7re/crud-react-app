import { useState } from 'react'

const AddUserForm = (props) => {

    const initUser = { name: '', email: '', login: '', phone: '', age: '', address: '' }

    const [user, setUser] = useState(initUser)

    const handleChange = e => {
        const { name, value } = e.target
        setUser({ ...user, [name]: value })
    }

    const handleSubmit = e => {
        e.preventDefault()
        handleChange(e, props.addUser(user))
        setUser(initUser)

    }

    return (
        <form>
            <label>Name</label>
            <input className="form-control" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Email</label>
            <input className="form-control" type="text" value={user.email} name="email" onChange={handleChange} />
            <label>Login</label>
            <input className="form-control" type="text" value={user.login} name="login" onChange={handleChange} />
            <label>Phone</label>
            <input className="form-control" type="text" value={user.phone} name="phone" onChange={handleChange} />
            <label>Age</label>
            <input className="form-control" type="text" value={user.age} name="age" onChange={handleChange} />
            <label>Adress</label>
            <input className="form-control" type="text" value={user.address} name="address" onChange={handleChange} />
            <button className="btn btn-primary" type="submit" onClick={handleSubmit} >Add user</button>
        </form>
    )
}

export default AddUserForm